//
//  QuizResultViewController.h
//  wp_quiz
//
//  Created by Tomasz Bilski on 17.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ParentControllerProtocol.h"

@interface QuizResultViewController : UIViewController

@property (nonatomic, strong) NSNumber *result;

@property (nonatomic, weak) id<ParentControllerProtocol>delegate;

@end

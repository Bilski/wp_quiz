//
//  QuizItemDetailsViewController.h
//  wp_quiz
//
//  Created by jonasz on 17.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ParentControllerProtocol.h"

@interface QuizItemDetailsViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, ParentControllerProtocol>

@property long identifier;

@property (nonatomic, weak) id<ParentControllerProtocol>delegate;

@end

//
//  QuizRequestBuilder.m
//  wp_quiz
//
//  Created by jonasz on 14.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import "QuizRequestBuilder.h"
#import "DefaultGetRequest.h"

@implementation QuizRequestBuilder

- (DefaultGetRequest *)getAllQuizzesListRequest {
    DefaultGetRequest *getRequest = [[DefaultGetRequest alloc] initWithServiceName:@"/v1/quizzes/0/100" andExtraParameters:@""];
    return getRequest;
}

- (DefaultGetRequest *)getSingleQuizRequest:(NSNumber *)quizId {
    NSString *extraParams=[NSString stringWithFormat:@"%@/0",quizId];
    DefaultGetRequest *getRequest = [[DefaultGetRequest alloc] initWithServiceName:@"/v1/quiz/" andExtraParameters:extraParams];
    return getRequest;
}

@end

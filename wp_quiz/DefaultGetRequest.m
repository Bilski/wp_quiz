//
//  DefaultGetRequest.m
//  wp_quiz
//
//  Created by jonasz on 14.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import "DefaultGetRequest.h"
#import "ConfigurationHolder.h"

@interface DefaultGetRequest()

@property (nonatomic) float conectionTimeout;
@property (nonatomic,strong) NSString *urlString;

@end

@implementation DefaultGetRequest

- (instancetype)initWithServiceName:(NSString *)serviceName andExtraParameters:(NSString *)extraParams {
    
    self = [super init];
    if (self) {
        [self setConectionTimeout:connectionTimeout];
        self.urlString = [self generateUrlFromHost:hostURL serviceName:serviceName andExtraParams:extraParams];
    }
    return self;
}

- (NSString *)generateUrlFromHost:(NSString *)hostString serviceName:(NSString *)serviceName andExtraParams:(NSString *)extraParams {
    return [[NSString alloc] initWithFormat:@"%@%@%@", hostString, serviceName, extraParams];
}

#pragma mark - NetworkDataTaskRequestProtocol methods

-(NSURLRequest *)urlRequest {
    NSURL *url = [NSURL URLWithString:self.urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:self.conectionTimeout];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"GET"];
    
    return request;
}

@end

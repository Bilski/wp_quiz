//
//  ParseProtocol.h
//  wp_quiz
//
//  Created by jonasz on 16.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ParseProtocol <NSObject>

- (void)updateFromDictionary:(NSDictionary *)dictionary;

@end

//
//  Answer.m
//  wp_quiz
//
//  Created by jonasz on 16.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import "Answer.h"

#import "KZPropertyMapper.h"

@implementation Answer

// Specify default values for properties

//+ (NSDictionary *)defaultPropertyValues
//{
//    return @{};
//}

// Specify properties to ignore (Realm won't persist these)

//+ (NSArray *)ignoredProperties
//{
//    return @[];
//}

- (void)updateFromDictionary:(NSDictionary *)dictionary {
    
    NSString *str;
    id textValue = [dictionary objectForKey:@"text"];
    if (![textValue isKindOfClass:[NSString class]]) {
        str = [textValue stringValue];
    } else {
        str = textValue;
    }
    self.text = str;
    
    
    
    [KZPropertyMapper mapValuesFrom:dictionary toInstance:self usingMapping:@{
                                                                                            @"order":KZProperty(order),
                                                               /*                             @"text":KZBox(stringValue, text),
                                                               */                             @"isCorrect":KZProperty(isCorrect)
                                                                                            }];
    
}

@end

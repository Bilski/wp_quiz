//
//  QuizItemTableViewCell.m
//  wp_quiz
//
//  Created by jonasz on 16.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import "QuizItemTableViewCell.h"

@implementation QuizItemTableViewCell

- (void)setTitleOnTheCell:(NSString *)title {
    self.title.text = title;
}

- (void)setTextDetailsOnTheCell:(NSString *)details {
    self.details.text = details;
    self.details.textColor = [UIColor orangeColor];
}

@end

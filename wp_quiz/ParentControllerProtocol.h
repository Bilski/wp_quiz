//
//  ParentControllerProtocol.h
//  wp_quiz
//
//  Created by Tomasz Bilski on 17.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ParentControllerProtocol <NSObject>

- (void)refreshData;

@end

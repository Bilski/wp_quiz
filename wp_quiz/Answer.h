//
//  Answer.h
//  wp_quiz
//
//  Created by jonasz on 16.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import <Realm/Realm.h>

#import "ParseProtocol.h"

@interface Answer : RLMObject<ParseProtocol>

@property NSString *text;
@property int order;
@property int isCorrect;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<Answer *><Answer>
RLM_ARRAY_TYPE(Answer)

//
//  Question.h
//  wp_quiz
//
//  Created by jonasz on 16.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import <Realm/Realm.h>

#import "Answer.h"

@interface Question : RLMObject<ParseProtocol>

@property RLMArray<Answer *><Answer> *answers;
@property NSString *text;
@property NSString *answer;
@property NSString *type;
@property int order;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<Question *><Question>
RLM_ARRAY_TYPE(Question)

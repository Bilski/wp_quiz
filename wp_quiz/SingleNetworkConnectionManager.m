//
//  SingleNetworkConnectionManager.m
//  wp_quiz
//
//  Created by Tomasz Bilski on 14.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import "SingleNetworkConnectionManager.h"

@interface SingleNetworkConnectionManager()

@property (nonatomic,strong) NSURLSessionDataTask *postDataTask;

@end

@implementation SingleNetworkConnectionManager

- (void)makeRequestWith:(id<NetworkDataTaskRequestProtocol>)provider completionHandler:(void (^)(id result,NSURLResponse *response,NSError *error))completionHandler {
    if (self.postDataTask) {
        completionHandler(nil,nil,[NSError new]);
    } else {
        NSURLRequest *req=[provider urlRequest];
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:Nil];
        _postDataTask = [session dataTaskWithRequest:req completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            id json;
            if(data && !error){
                json=[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            }
            if (error) {}
            completionHandler(json,response,error);
        }];
        [_postDataTask resume];
    }
}

- (void)cancelRequests {
    if (self.postDataTask.state == NSURLSessionTaskStateRunning) {
        [self.postDataTask cancel];
    }
}

@end

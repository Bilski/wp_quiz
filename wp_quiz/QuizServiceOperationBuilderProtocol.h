//
//  QuizServiceOperationBuilderProtocol.h
//  wp_quiz
//
//  Created by Tomasz Bilski on 14.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

@class ObtainAllQuizzesOperation;
@class ObtainSingleQuizOperation;

@protocol QuizServiceOperationBuilderProtocol <NSObject>

- (ObtainAllQuizzesOperation *)getAllQuizzes:(void (^)())completionHandler errorHandler:(void (^)(id error))errorHandler;
- (ObtainSingleQuizOperation *)getSingleQuizWithId:(NSNumber *)quizId completionHandler:(void (^)())completionHandler errorHandler:(void (^)(id error))errorHandler;

@end

//
//  QuizListViewController.m
//  wp_quiz
//
//  Created by jonasz on 14.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>

#import "QuizListViewController.h"
#import "QuizServiceOperationBuilderProtocol.h"
#import "QuizServiceOperationBuilder.h"
#import "ConcurrentOperation.h"
#import "ObtainAllQuizzesOperation.h"
#import "ObtainSingleQuizOperation.h"
#import "QuizList.h"
#import "QuizItemTableViewCell.h"
#import "QuizItemDetailsViewController.h"

@interface QuizListViewController ()

@property (strong, nonatomic) IBOutlet UITableView *quizzesList;
@property (nonatomic, strong) RLMResults<QuizList *> *quizzes;

@property (strong) NSOperationQueue *queue;
@property (nonatomic, strong) NSObject<QuizServiceOperationBuilderProtocol> *quizReqestBuilder;

@end

@implementation QuizListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Wybierz quiz";
    
    self.quizReqestBuilder = [QuizServiceOperationBuilder new];
    
    [self updateQuizData];
    
    if ([self shouldLoadQuizzess]) {
       [self loadAllQuizzes];
    }
    
    self.quizzesList.estimatedRowHeight = 150;
    self.quizzesList.rowHeight = UITableViewAutomaticDimension;
}


- (BOOL)shouldLoadQuizzess {
    return [self.quizzes count] <= 0 ? YES : NO;
}

- (void)loadAllQuizzes {
    __weak typeof(self) weakMe = self;
    ConcurrentOperation *op = [self loadAllQuizzesOperation];
    op.ProcessResult = ^(id result) {
        [weakMe addObjectToRealm:result];
    };
    [[NSOperationQueue new] addOperation:op];
}

- (void)addObjectToRealm:(id)object {
    QuizList *ql = object;
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    [realm beginWriteTransaction];
    [realm addObject:ql];
    [realm commitWriteTransaction];
    
    [self updateQuizData];
    [self updateQuizDataOnViews];

}

- (void)updateQuizDataOnViews {
    [self.quizzesList reloadData];
}

- (void)updateQuizData {
    self.quizzes = [QuizList allObjects];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"goToQuiz"]) {
        QuizItemDetailsViewController *detailsController = segue.destinationViewController;
        QuizItem *qi = [self.quizzes[0].items objectAtIndex:self.quizzesList.indexPathForSelectedRow.row];
        detailsController.identifier = qi.identifier;
        detailsController.delegate = self;
    }
}


#pragma mark - UITableViewDataSource and UITableViewDelegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.quizzes count] > 0 ? [self.quizzes[0].items count] : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    QuizItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"quizItem"];
    
    QuizItem *qi = [self.quizzes[0].items objectAtIndex:indexPath.row];
    
    
    [cell setTitleOnTheCell:qi.title];
    [cell setTextDetailsOnTheCell:nil];
    
    if (qi.questionIndex == 0) {
        if (qi.result) {
            //pokaz ostatni wynik
            NSString *resultText = [NSString stringWithFormat:@"Ostatni wynik: %@%%",qi.result];
            [cell setTextDetailsOnTheCell:resultText];
        }
    } else {
        //pokaz postep
        float resultInPercent = ((float)qi.questionIndex / (float)qi.questions) *100;
        NSNumber *progress = [NSNumber numberWithInteger:resultInPercent];
        NSString *progressText = [NSString stringWithFormat:@"Quiz rozwiązany w: %@%%",progress];
        [cell setTextDetailsOnTheCell:progressText];
    }
    
    [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:qi.thumbnailUrl]
                      placeholderImage:nil];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark - builder methods

- (ConcurrentOperation *)loadAllQuizzesOperation {
    ObtainAllQuizzesOperation *ops = [self.quizReqestBuilder getAllQuizzes:^{} errorHandler:^(id error) {}];
    return ops;
}

#pragma mark - ParentControllerProtocol method

- (void)refreshData {
    [self updateQuizData];
    [self updateQuizDataOnViews];
}

@end
    
    

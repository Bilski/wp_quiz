//
//  QuizRequestProtocol.h
//  wp_quiz
//
//  Created by jonasz on 14.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DefaultGetRequest;

@protocol QuizRequestProtocol <NSObject>

- (DefaultGetRequest *)getAllQuizzesListRequest;
- (DefaultGetRequest *)getSingleQuizRequest:(NSNumber *)quizId;

@end

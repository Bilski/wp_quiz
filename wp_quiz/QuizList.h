//
//  QuizList.h
//  wp_quiz
//
//  Created by Tomasz Bilski on 15.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import <Realm/Realm.h>

#import "QuizItem.h"

@interface QuizList : RLMObject<ParseProtocol>

@property RLMArray<QuizItem *><QuizItem> *items;
@property int count;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<QuizList *><QuizList>
RLM_ARRAY_TYPE(QuizList)

//
//  NetworkManagerProtocol.h
//  wp_quiz
//
//  Created by jonasz on 14.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import "NetworkDataTaskRequestProtocol.h"

@protocol NetworkManagerProtocol <NSObject>

- (void)makeRequestWith:(id<NetworkDataTaskRequestProtocol>)provider completionHandler:(void (^)(id result,NSURLResponse *response,NSError *error))completionHandler;
- (void)cancelRequests;

@end

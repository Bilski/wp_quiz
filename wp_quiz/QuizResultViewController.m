//
//  QuizResultViewController.m
//  wp_quiz
//
//  Created by Tomasz Bilski on 17.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import "QuizResultViewController.h"

@interface QuizResultViewController ()

@property (weak, nonatomic) IBOutlet UILabel *resultLabel;

@end

@implementation QuizResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *resultInText = [NSString stringWithFormat:@"%@%%",self.result];
    self.resultLabel.text = resultInText;
}

- (IBAction)goToQuizzesList:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"quizListViewController"];
    [self presentViewController:vc animated:YES completion:NULL];
}

- (IBAction)puzzleOnceMore:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if ([self.delegate respondsToSelector:@selector(refreshData)]) {
        [self.delegate refreshData];
    }
}

@end

//
//  QuizServiceOperationBuilder.h
//  wp_quiz
//
//  Created by Tomasz Bilski on 14.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "QuizServiceOperationBuilderProtocol.h"

@interface QuizServiceOperationBuilder : NSObject<QuizServiceOperationBuilderProtocol>

@end

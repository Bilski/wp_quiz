//
//  DefaultGetRequest.h
//  wp_quiz
//
//  Created by jonasz on 14.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "NetworkDataTaskRequestProtocol.h"

@interface DefaultGetRequest : NSObject<NetworkDataTaskRequestProtocol>

- (instancetype)initWithServiceName:(NSString *)serviceName andExtraParameters:(NSString *)extraParams;

@end

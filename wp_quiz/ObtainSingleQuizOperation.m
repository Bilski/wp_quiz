//
//  ObtainSingleQuizOperation.m
//  wp_quiz
//
//  Created by Tomasz Bilski on 14.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import "ObtainSingleQuizOperation.h"

#import "QuizRequestProtocol.h"
#import "QuizRequestBuilder.h"
#import "DefaultGetRequest.h"

@implementation ObtainSingleQuizOperation

- (id<NetworkDataTaskRequestProtocol>)downloadRequest {
    id<QuizRequestProtocol> builder = [QuizRequestBuilder new];
    return [builder getSingleQuizRequest:self.quizId];
}

@end

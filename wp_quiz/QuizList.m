//
//  QuizList.m
//  wp_quiz
//
//  Created by Tomasz Bilski on 15.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import "QuizList.h"

#import "KZPropertyMapper.h"


@implementation QuizList

// Specify default values for properties

//+ (NSDictionary *)defaultPropertyValues
//{
//    return @{};
//}

// Specify properties to ignore (Realm won't persist these)

//+ (NSArray *)ignoredProperties
//{
//    return @[];
//}

- (void)updateFromDictionary:(NSDictionary *)dictionary {
    [KZPropertyMapper mapValuesFrom:dictionary toInstance:self usingMapping:@{@"count": KZProperty(count)}];
    
}

@end

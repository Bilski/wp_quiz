//
//  Question.m
//  wp_quiz
//
//  Created by jonasz on 16.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import "Question.h"

#import "KZPropertyMapper.h"

@implementation Question

// Specify default values for properties

//+ (NSDictionary *)defaultPropertyValues
//{
//    return @{};
//}

// Specify properties to ignore (Realm won't persist these)

//+ (NSArray *)ignoredProperties
//{
//    return @[];
//}

- (void)updateFromDictionary:(NSDictionary *)dictionary {
    
    
    [KZPropertyMapper mapValuesFrom:dictionary toInstance:self usingMapping:@{
                                                                                            @"text":KZProperty(text),
                                                                                            @"answer":KZProperty(answer),
                                                                                            @"type":KZProperty(type),
                                                                                            @"order":KZProperty(order)
                                                                                            }];

}

@end

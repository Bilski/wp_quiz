//
//  QuizItemDetails.h
//  wp_quiz
//
//  Created by jonasz on 16.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import <Realm/Realm.h>

#import "ParseProtocol.h"

#import "Question.h"

@interface QuizItemDetails : RLMObject<ParseProtocol>

@property long identifier;
@property int questionIndex;
@property int numberOfCorrectAnswers;
@property NSString *title;

@property RLMArray<Question *><Question> *questions;

- (void)update:(void (^)(QuizItemDetails *instance))updateBlock;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<QuizItemDetails *><QuizItemDetails>
RLM_ARRAY_TYPE(QuizItemDetails)

//
//  QuizItem.h
//  wp_quiz
//
//  Created by jonasz on 15.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import <Realm/Realm.h>

#import "ParseProtocol.h"

@class MainPhoto;

@interface QuizItem : RLMObject<ParseProtocol>

@property long identifier;
@property int questions;
@property int questionIndex;
@property NSString *result;
@property NSString *title;
@property NSString *content;
@property NSString *thumbnailUrl;

- (void)update:(void (^)(QuizItem *instance))updateBlock;
- (void)incrementQuestionIndex;
- (void)resetQuestionIndex;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<QuizItem *><QuizItem>
RLM_ARRAY_TYPE(QuizItem)

//
//  QuizServiceOperationBuilder.m
//  wp_quiz
//
//  Created by Tomasz Bilski on 14.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import "QuizServiceOperationBuilder.h"

#import "ConfigurationHolder.h"

#import "ObtainAllQuizzesOperation.h"
#import "ObtainSingleQuizOperation.h"

#import "QuizItem.h"

@implementation QuizServiceOperationBuilder

- (ObtainAllQuizzesOperation *)getAllQuizzes:(void (^)())completionHandler errorHandler:(void (^)(id))errorHandler {
    
    ObtainAllQuizzesOperation *op = [ObtainAllQuizzesOperation new];
    
    op.OperationFailureHandler = errorHandler;
    op.ProcessResult = completionHandler;
    op.operationType = ObtainAllQuizzes;
    
    return op;
}

- (ObtainSingleQuizOperation *)getSingleQuizWithId:(NSNumber *)quizId completionHandler:(void (^)())completionHandler errorHandler:(void (^)(id))errorHandler {
    
    ObtainSingleQuizOperation *op = [ObtainSingleQuizOperation new];
    
    op.OperationFailureHandler = errorHandler;
    op.ProcessResult = completionHandler;
    op.operationType = ObtainSingleQuiz;
    op.quizId = quizId;
    
    return op;
}

@end

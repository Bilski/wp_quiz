//
//  QuizItemTableViewCell.h
//  wp_quiz
//
//  Created by jonasz on 16.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuizItemTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *title;
@property (strong, nonatomic) IBOutlet UILabel *details;
@property (strong, nonatomic) IBOutlet UIImageView *thumbnail;

- (void)setTitleOnTheCell:(NSString *)title;
- (void)setTextDetailsOnTheCell:(NSString *)details;

@end

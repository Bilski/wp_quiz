//
//  ConfigurationHolder.h
//  wp_quiz
//
//  Created by jonasz on 14.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConfigurationHolder : NSObject

//networking
extern NSString * const hostURL;
extern float const connectionTimeout;

typedef NS_ENUM(NSInteger, OperationType) {
    ObtainAllQuizzes = 0,
    ObtainSingleQuiz
};

@end

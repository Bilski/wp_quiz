//
//  ConcurrentOperation.m
//  wp_quiz
//
//  Created by jonasz on 14.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import "ConcurrentOperation.h"
#import "SingleNetworkConnectionManager.h"

#import "QuizItem.h"
#import "QuizList.h"
#import "QuizItemDetails.h"
#import "Question.h"
#import "Answer.h"

@implementation ConcurrentOperation

- (instancetype)init {
    self = [super init];
    if (self) {
        executing = NO;
        finished = NO;
    }
    return self;
}

- (BOOL)isConcurrent {
    return YES;
}
- (BOOL)isExecuting {
    return executing;
}
- (BOOL)isFinished {
    return finished;
}

- (void)start {
    if([self isCancelled]) {
        [self willChangeValueForKey:@"isFinished"];
        finished = YES;
        [self didChangeValueForKey:@"isFinished"];
        return;
    }
    [self willChangeValueForKey:@"isExecuting"];
    [NSThread detachNewThreadSelector:@selector(main) toTarget:self withObject:nil];
    executing = YES;
    [self didChangeValueForKey:@"isExecuting"];
}

- (void)completeOperation {
    [self willChangeValueForKey:@"isFinished"];
    [self willChangeValueForKey:@"isExecuting"];
    executing = NO;
    finished = YES;
    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];
}

- (void)completeOperationWithFailure {
    [self cancel];
    [self.manager cancelRequests];
    [self completeOperation];
}

- (void)main {
    @autoreleasepool {
        if([self isCancelled]){
            [self completeOperationWithFailure];
            return;
        }
        [self performOperation];
    }
}

-(void)performOperation {
    _manager = [self downloadManager];
    id<NetworkDataTaskRequestProtocol> provider = [self downloadRequest];
    
    [_manager makeRequestWith:provider completionHandler:^(id data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^() {
            NSInteger code = [(NSHTTPURLResponse *)response statusCode];
            if (!response) code = -1;
            if( error || code >= 300 || code == -1) {
                [self operationFailed:error withData:data andCode:code];
                
                [self completeOperationWithFailure];
            } else {
                [self operationSuceded:data];
                [self completeOperation];
                
            }
        });
        
    }];
}

- (void)operationFailed:(NSError *)error withData:(id)response andCode:(NSInteger)code {
    if (error.code == NSURLErrorCancelled) {
        return;
    }
    if (self.OperationFailureHandler) {
        self.OperationFailureHandler(error);
    }
}

- (void)operationSuceded:(id)data {
    if(self.ProcessResult){
        self.ProcessResult([self parseRecivedData:data]);
    }
}

- (id)parseRecivedData:(id)data {
    
    id parsedData;
    
    switch (self.operationType) {
        case ObtainSingleQuiz:
            parsedData = [self parseDataForSingleQuiz:data];
            break;
        case ObtainAllQuizzes:
            parsedData = [self parseDataForAllQuizzes:data];
            break;
        default:
            break;
    }

    return parsedData;
}

- (id<NetworkDataTaskRequestProtocol>)downloadRequest {
    return nil;
}

- (id<NetworkManagerProtocol>)downloadManager {
    return [SingleNetworkConnectionManager new];
}

#pragma mark - parse methods

- (id)parseDataForSingleQuiz:(id)data {
    QuizItemDetails *qid = [[QuizItemDetails alloc] init];
    [qid updateFromDictionary:data];
    
    [self insertQuestions:qid withData:data[@"questions"]];
    
    return qid;
    
}

- (id)parseDataForAllQuizzes:(id)data {
    QuizList *ql = [[QuizList alloc] init];
    [ql updateFromDictionary:data];
    
    NSArray *items = [data objectForKey:@"items"];
    
    for (NSDictionary *i in items) {
        QuizItem *qi = [[QuizItem alloc] init];
        [qi updateFromDictionary:i];
        [ql.items addObject:qi];
    }
    
    return ql;
}

- (void)insertQuestions:(QuizItemDetails *)qid withData:(id)data {
    for (NSDictionary *it in data) {
        Question *question = [[Question alloc] init];
        [question updateFromDictionary:it];
        
        [self insertAnswers:question withData:it[@"answers"]];
        
        [qid.questions addObject:question];
        
    }
    
}

- (void)insertAnswers:(Question *)question withData:(id)data {
    for (NSDictionary *j in data) {
        Answer *answer = [[Answer alloc] init];
        [answer updateFromDictionary:j];
        
        [question.answers addObject:answer];
    }
}

@end

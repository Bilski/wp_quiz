//
//  QuizItem.m
//  wp_quiz
//
//  Created by jonasz on 15.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import "QuizItem.h"

#import "KZPropertyMapper.h"

@implementation QuizItem

// Specify default values for properties

//+ (NSDictionary *)defaultPropertyValues {
//    return @{};
//}

// Specify properties to ignore (Realm won't persist these)

//+ (NSArray *)ignoredProperties
//{
//    return @[];
//}

- (void)updateFromDictionary:(NSDictionary *)dictionary {
    [KZPropertyMapper mapValuesFrom:dictionary toInstance:self usingMapping:@{@"id": KZProperty(identifier),
                                                                              @"title": KZProperty(title),
                                                                              @"questions" : KZProperty(questions),
                                                                              @"content": KZProperty(content),
                                                                              @"mainPhoto": @{
                                                                                      @"url": KZProperty(thumbnailUrl)
                                                                                      },
                                                                              }];
}

- (void)update:(void (^)(QuizItem *instance))updateBlock {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    updateBlock(self);
    [realm commitWriteTransaction];
}

- (void)incrementQuestionIndex {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    self.questionIndex++;
    [realm commitWriteTransaction];
}

- (void)resetQuestionIndex {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    self.questionIndex = 0;
    [realm commitWriteTransaction];
}

@end

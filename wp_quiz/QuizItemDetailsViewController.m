//
//  QuizItemDetailsViewController.m
//  wp_quiz
//
//  Created by jonasz on 17.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import "QuizItemDetailsViewController.h"

#import "QuizItemDetails.h"
#import "QuizItem.h"
#import "QuizServiceOperationBuilderProtocol.h"
#import "ConcurrentOperation.h"
#import "ObtainSingleQuizOperation.h"
#import "QuizServiceOperationBuilder.h"
#import "QuizResultViewController.h"

@interface QuizItemDetailsViewController ()

@property (nonatomic, strong) NSObject<QuizServiceOperationBuilderProtocol> *quizReqestBuilder;

@property QuizItemDetails *quiz;

@property (strong, nonatomic) IBOutlet UILabel *quizTitle;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UITableView *quizTable;
@property (weak, nonatomic) IBOutlet UILabel *progressLabel;

@end

@implementation QuizItemDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.quizReqestBuilder = [QuizServiceOperationBuilder new];
    
    [self fetchQuiz];
    if (!self.quiz) [self downloadQuiz];
    [self updateQuizDataOnViews];
}

- (void)fetchQuiz {
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"identifier == %ld",self.identifier];
    RLMResults<QuizItemDetails *> *qi = [QuizItemDetails objectsWithPredicate:pred];
    if ([qi count] > 0) self.quiz = qi[0];
}

- (void)downloadQuiz {
    
    self.activityIndicator.hidden = NO;
    [self.activityIndicator startAnimating];
    
    __weak typeof(self) weakMe = self;
    ConcurrentOperation *op = [self loadSingleQuizOperation];
    op.ProcessResult = ^(id result) {
        [weakMe addObjectToRealm:result];
        [self fetchQuiz];
        [self updateQuizDataOnViews];
        [self.quizTable reloadData];
        [self.activityIndicator stopAnimating];
    };
    op.OperationFailureHandler = ^(id error) {
        [self.activityIndicator stopAnimating];
    };
    [[NSOperationQueue new] addOperation:op];
}

- (void)updateQuizDataOnViews {
    self.quizTitle.text = self.quiz.title;
    [self setTextOnProgressLabel];
}

- (void)addObjectToRealm:(id)object {
    QuizItemDetails *qid = object;
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    [realm beginWriteTransaction];
    [realm addObject:qid];
    [realm commitWriteTransaction];
}

- (void)reloadOptionAnswerSectionWithAnimation:(UITableViewRowAnimation)animation {
    [self.quizTable beginUpdates];
    [self.quizTable reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:animation];
    [self.quizTable endUpdates];
}

- (QuizItem *)fetchCorrespondingQuizItemObject {
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"identifier == %ld",self.identifier];
    RLMResults<QuizItem *> *qi = [QuizItem objectsWithPredicate:pred];
    
    return [qi count] > 0 ? qi[0] : nil;
}

- (void)setTextOnProgressLabel {
    
    int currentQuestionNumber = self.quiz.questionIndex + 1;
    int questionsNumber = (int)self.quiz.questions.count;
    
    self.progressLabel.text = [NSString stringWithFormat:@"Pytanie %d z %ld", currentQuestionNumber, (long)questionsNumber];
}

- (void)saveResult:(NSInteger)index {
    Question *question = [self.quiz.questions objectAtIndex:self.quiz.questionIndex];
    Answer *answer = question.answers[index];
    
    if (answer.isCorrect == 1) {
        [self.quiz update:^(QuizItemDetails *instance) {
            self.quiz.numberOfCorrectAnswers++;
        }];
    }
}

- (void)goToResults {
    [self performSegueWithIdentifier:@"goToResultScreen" sender:self];
}

#pragma mark - UITableViewDataSource and UITableViewDelegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    Question *question = [self.quiz.questions objectAtIndex:self.quiz.questionIndex];

    return [question.answers count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myIdentifier"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MyIdentifier"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    Question *question = [self.quiz.questions objectAtIndex:self.quiz.questionIndex];
    
    Answer *answer = question.answers[indexPath.row];
    
    cell.textLabel.text = answer.text;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self saveResult:indexPath.row];
    
    if (self.quiz.questionIndex < [self.quiz.questions count] - 1) {
        [self incrementQuestionIndex];
        [self reloadOptionAnswerSectionWithAnimation:UITableViewRowAnimationLeft];
        [self setTextOnProgressLabel];
    } else {
        [self goToResults];
        [self resetIndexes];
    }
    
    [self refreshDataOnParentController];
}

- (void)incrementQuestionIndex {
    [self.quiz update:^(QuizItemDetails *instance) {
        self.quiz.questionIndex++;
    }];
    
    QuizItem *qi = [self fetchCorrespondingQuizItemObject];
    if (qi) [qi incrementQuestionIndex];
}

- (void)resetIndexes {
    [self.quiz update:^(QuizItemDetails *instance) {
        self.quiz.questionIndex = 0;
        self.quiz.numberOfCorrectAnswers = 0;
    }];
    
    QuizItem *qi = [self fetchCorrespondingQuizItemObject];
    if (qi) [qi resetQuestionIndex];
}

- (void)refreshDataOnParentController {
    if ([self.delegate respondsToSelector:@selector(refreshData)]) {
        [self.delegate refreshData];
    }
}

//workaround to have lower case title in a header

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    if ([view isKindOfClass:[UITableViewHeaderFooterView class]] && [self respondsToSelector:@selector(tableView:titleForHeaderInSection:)]) {
        UITableViewHeaderFooterView *headerView = (UITableViewHeaderFooterView *)view;
        headerView.textLabel.text = [self tableView:tableView titleForHeaderInSection:section];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *titleHeader;
    
    Question *question = [self.quiz.questions objectAtIndex:self.quiz.questionIndex];
    
    titleHeader = question.text;
    
    return titleHeader;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"goToResultScreen"]) {
        QuizResultViewController *resultController = segue.destinationViewController;
        float resultInPercent = ((float)self.quiz.numberOfCorrectAnswers / (float)[self.quiz.questions count]) *100;
        NSNumber *resultInNumber = [NSNumber numberWithInteger:resultInPercent];
        resultController.result = resultInNumber;
        
        QuizItem *qi = [self fetchCorrespondingQuizItemObject];
        [qi update:^(QuizItem *instance) {
            qi.result = [resultInNumber stringValue];
        }];
        
        resultController.delegate = self;
    }
}

#pragma mark - builder methods

- (ConcurrentOperation *)loadSingleQuizOperation {
    NSNumber *identifier = [NSNumber numberWithLong:self.identifier];
    ObtainSingleQuizOperation *ops = [self.quizReqestBuilder getSingleQuizWithId:identifier completionHandler:^{} errorHandler:^(id error) {}];
    return ops;
}

#pragma mark - ParentControllerProtocol method

- (void)refreshData {
    [self.quizTable reloadData];
}

@end

//
//  ObtainSingleQuizOperation.h
//  wp_quiz
//
//  Created by Tomasz Bilski on 14.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import "ConcurrentOperation.h"

@interface ObtainSingleQuizOperation : ConcurrentOperation

@property (nonatomic, strong) NSNumber *quizId;

@end

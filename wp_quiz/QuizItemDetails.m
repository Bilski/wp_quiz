//
//  QuizItemDetails.m
//  wp_quiz
//
//  Created by jonasz on 16.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import "QuizItemDetails.h"

#import "KZPropertyMapper.h"

@implementation QuizItemDetails

// Specify default values for properties

+ (NSDictionary *)defaultPropertyValues
{
    return @{@"questionIndex" : @0,
             @"numberOfCorrectAnswers" : @0};
}

// Specify properties to ignore (Realm won't persist these)

//+ (NSArray *)ignoredProperties
//{
//    return @[];
//}

- (void)update:(void (^)(QuizItemDetails *instance))updateBlock {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    updateBlock(self);
    [realm commitWriteTransaction];
}

- (void)updateFromDictionary:(NSDictionary *)dictionary {
    [KZPropertyMapper mapValuesFrom:dictionary toInstance:self usingMapping:@{@"id": KZProperty(identifier),
                                                                              @"title":KZProperty(title),
                                                                              }];
}

@end

//
//  QuizListViewController.h
//  wp_quiz
//
//  Created by jonasz on 14.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ParentControllerProtocol.h"

@interface QuizListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, ParentControllerProtocol>

@end

//
//  ConcurrentOperation.h
//  wp_quiz
//
//  Created by jonasz on 14.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ConfigurationHolder.h"

#import "NetworkDataTaskRequestProtocol.h"
#import "NetworkManagerProtocol.h"

@interface ConcurrentOperation : NSOperation {
    BOOL executing;
    BOOL finished;
}

@property (nonatomic) id<NetworkManagerProtocol> manager;

@property (nonatomic) OperationType operationType;

@property (nonatomic,copy) void (^ProcessResult)(id result);
@property (nonatomic,copy) void (^OperationFailureHandler)(id error);

- (id<NetworkDataTaskRequestProtocol>)downloadRequest;

@end

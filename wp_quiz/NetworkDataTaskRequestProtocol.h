//
//  NetworkDataTaskRequestProtocol.h
//  wp_quiz
//
//  Created by jonasz on 14.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol NetworkDataTaskRequestProtocol <NSObject>
- (NSURLRequest *)urlRequest;
@end

//
//  ConfigurationHolder.m
//  wp_quiz
//
//  Created by jonasz on 14.03.2017.
//  Copyright © 2017 jonasz. All rights reserved.
//

#import "ConfigurationHolder.h"

@implementation ConfigurationHolder

NSString * const hostURL = @"http://quiz.o2.pl/api";
float const connectionTimeout = 300.0f;

@end
